import math._
import scala.annotation.tailrec
import scala.util._
import Const._

/**
 * Save humans, destroy zombies!
 **/
object Player extends App {

  // game loop
  var iterationCount = 0
  while(true) {
    iterationCount += 1
    val Array(x, y) = for(i <- readLine split " ") yield i.toInt
    val hero = Hero(x, y)

    val humancount = readInt
    val humans = for(i <- 0 until humancount) yield {
      val Array(humanid, humanx, humany) = for(i <- readLine split " ") yield i.toInt
      Human(humanid, humanx, humany)
    }
    val zombiecount = readInt
    val zombies = for(i <- 0 until zombiecount) yield {
      val Array(zombieid, zombiex, zombiey, zombiexnext, zombieynext) = for(i <- readLine split " ") yield i.toInt
      Zombie(zombieid, zombiex, zombiey/*, zombiexnext, zombieynext*/)
    }

    val state = State(hero = hero, humans = humans.toList, zombies = zombies.toList, score = 0)
    val startTime = System.currentTimeMillis
    val(heroX, heroY) = State.strategy(state, startTime)

    println(heroX.toString + " " + heroY.toString) // Your destination coordinates
  }
}

object Const {
  val HARD_TIME_LIMIT = 90
  val MAX_TIME = 80
  val MAX_DEPTH = 100
  val MAX_STEPS = 2000
  val HUMAN_A_MAX = 5
  val ZOMBIE_A_MAX_NEAR_HERO = 5
  val ZOMBIE_A_MAX_NEAR_HUMAN = 5

  val HERO_SPEED = 1000
  val ZOMBIE_SPEED = 400
  val HERO_ATTACK_DIST = 2000
  val ZOMBIE_ATTACK_DIST = 400
  val MAX_DIST = Math.sqrt(9000 * 9000 + 16000 * 16000)
  val MAX_DIST_SQR = MAX_DIST * MAX_DIST

  val HERO_SPEED_SQR = HERO_SPEED * HERO_SPEED
  val ZOMBIE_SPEED_SQR = ZOMBIE_SPEED * ZOMBIE_SPEED
  val HERO_ATTACK_DIST_SQR = HERO_ATTACK_DIST * HERO_ATTACK_DIST
  val ZOMBIE_ATTACK_DIST_SQR = ZOMBIE_ATTACK_DIST * ZOMBIE_ATTACK_DIST

  // N-th equals fib(N + 1), N no more then ~ 30
  val SCORE_MULT = Map(1 -> 1, 2 -> 2, 3 -> 3, 4 -> 5, 5 -> 8, 6 -> 13, 7 -> 21, 8 -> 34, 9 -> 55, 10 -> 89, 11 -> 144,
    12 -> 233, 13 -> 377, 14 -> 610, 15 -> 987, 16 -> 1597, 17 -> 2584, 18 -> 4181, 19 -> 6765, 20 -> 10946,
    21 -> 17711, 22 -> 28657, 23 -> 46368, 24 -> 75025, 25 -> 121393, 26 -> 196418, 27 -> 317811, 28 -> 514229,
    29 -> 832040, 30 -> 1346269)
}

case class State(hero: Hero, humans: List[Human], zombies: List[Zombie], score: Int) {

  @inline
  def next(target: Creature): State = next(target.x, target.y)

  def next(targetX: Int, targetY: Int): State = {
    val newZombies = updateZombies()
    val newHero = updateHero(targetX, targetY)
    val (survivedZombies, score) = heroAttack(newHero, newZombies)
    val newHumans = zombieAttack(survivedZombies)
    State(hero = newHero, humans = newHumans, zombies = survivedZombies, score = if (newHumans.isEmpty) 0 else score)
  }

  private def updateZombies(): List[Zombie] = {
    val allHumans: List[Creature] = hero :: humans
    zombies.map {
      z =>
        var target: Creature = hero
        var distSquare = z.distSquare(hero)
        humans.foreach {
          h =>
            val newDistSquare = z.distSquare(h)
            if (newDistSquare < distSquare) {
              target = h
              distSquare = newDistSquare
            }
        }
        z.move(target)
    }
  }

  private def updateHero(targetX: Int, targetY: Int): Hero = hero.move(targetX, targetY)

  private def heroAttack(hero: Hero, zombies: List[Zombie]): (List[Zombie], Int) = {
    val survivedZombies = zombies.filter { z =>
      hero.distSquare(z) > HERO_ATTACK_DIST
    }
    val zombiesKilled = zombies.size - survivedZombies.size
    val score =
      if (zombiesKilled == 0)
        0
      else if (zombiesKilled == 1)
        humans.size * 10
      else {
        val base = humans.size * 10
        (1 to zombiesKilled).map {
          nth =>
            base * scoreMult(nth)
        }.sum
      }
    (survivedZombies, score)
  }

  private def zombieAttack(zombies: List[Zombie]): List[Human] =
    humans.filter {
      h =>
        !zombies.exists(_.distSquare(h) <= ZOMBIE_ATTACK_DIST_SQR)
    }

  private def scoreMult(n: Int) = SCORE_MULT(n)

  def grade(): Double =
    if (humans.isEmpty)
      Int.MinValue
    else {
      var mass = 0D
      var killable = 0D
      for (h <- humans) {
        mass += MAX_DIST / h.distSquare(hero)
        for (z <- zombies) {
          val z2 = z.move(h)
          if (z2.canAttack(h) && !(hero.canAttack(z2) || hero.canAttack(z)))
            killable -= (if (humans.size > 1) (2 / humans.size) else 100000000)
        }
      }
      score + humans.size - zombies.size * 0.1 + killable
    }

  override def toString = s"[hero=$hero, score=$score(${grade()}), zombies=$zombies, humans=${humans}]"
}

case class Step(state: State, target: Creature, prev: Option[Step]) {
  def grade(): Double = state.grade()

  override def toString = s"\n\tStep[\n\t\tstate=${state}\n\t\ttarget=${target}\n\t\tprev=${prev}"
}

object State {
  def variants(prev: Step): List[Step] = {
    val state = prev.state
    val analyzedHumans = Creature.selectNearest(state.hero, state.humans, HUMAN_A_MAX)
    val analyzedZombies = (Creature.selectNearest(state.hero, state.zombies, ZOMBIE_A_MAX_NEAR_HERO) ++
      state.humans.flatMap { h =>
        Creature.selectNearest(h, state.zombies, ZOMBIE_A_MAX_NEAR_HUMAN)
      }).groupBy(_.id).values.flatten
    val toCreature = (analyzedHumans ++ analyzedZombies).map {
      c => Step(state.next(c), c, Some(prev))
    }
    toCreature
  }

  def selectBest(states: List[Step], timeExceeded: => Boolean, checkTime: Boolean): Option[List[Step]] = {
    if (states.isEmpty)
      None
    else {
      var otherBest: List[Step] = Nil
      var tail: List[Step] = states.tail
      var best: Step = states.head
      var bestGrade: Double = best.grade()
      var i: Int = 0
      while(tail.nonEmpty) {
        i += 1
        if (checkTime && i % 10 == 0 && timeExceeded)
          return None
        val s = tail.head
        tail = tail.tail
        val sGrade = s.grade()
        if (sGrade > bestGrade) {
          best = s
          bestGrade = sGrade
          otherBest = Nil
        } else if (sGrade == bestGrade) {
          otherBest = s :: otherBest
        }
      }
      Some(best :: otherBest)
    }
  }

  def strategy(state: State, startTime: Long): (Int, Int) = {
    @inline
    def timeExceeded = {
      System.currentTimeMillis - startTime > HARD_TIME_LIMIT
    }

    @tailrec
    def go(steps: List[Step], depth: Int, alreadyResult: List[Step]): List[Step] = {
      Console.err.println(s"****** analyze steps (time=${System.currentTimeMillis - startTime}, size=${steps.size}, depth=${depth}})")

      val allVariants =
        if (steps.size < 10) steps.flatMap(variants)
        else {
          var vs: List[Step] = Nil
          var iter = steps.iterator
          var i: Int = 0
          while (iter.hasNext) {
            i += 1
            if (i % 10 == 0 && alreadyResult.nonEmpty && timeExceeded)
              return alreadyResult
            vs = variants(iter.next) ::: vs
          }
          vs
        }
      if (alreadyResult.nonEmpty && timeExceeded)
        return alreadyResult
      val allVariantsUniq = allVariants.toSet.toList
      if (alreadyResult.nonEmpty && timeExceeded)
        return alreadyResult
      val steps2Opt = selectBest(allVariantsUniq, timeExceeded, alreadyResult.nonEmpty)
      if (steps2Opt.isEmpty)
        return alreadyResult
      val steps2 = steps2Opt.get
      val time = System.currentTimeMillis - startTime
      if (time > MAX_TIME || depth > MAX_DEPTH || steps2.size > MAX_STEPS) {
        Console.err.println(s"STOP analyze in case of time=${time > MAX_TIME}($time), depth=${depth > MAX_DEPTH}($depth), many steps=${steps2.size > MAX_STEPS}(${steps2.size})")
        steps2
      } else {
        go(steps2, depth + 1, steps)
      }
    }

    @tailrec
    def stepToInitial(step: Step): Step =
      if (step.prev.isEmpty ||  step.prev.get.prev.isEmpty)
        step
      else
        stepToInitial(step.prev.get)

    if (danger(state))
      return saveNearestStrategy(state)

    val best = go(Step(state, null, None) :: Nil, 1, Nil).head
    if (best.state.humans.isEmpty) {
      Console.err.println(s"\tBEST ${best}")
      return saveNearestStrategy(state)
    }
    Console.err.println(s"\tBEST ${best}")
    val Step(nextBestState, bestTarget, _) = stepToInitial(best)

    Console.err.println(s"\tGOTO x=${nextBestState.hero.x}, y=${nextBestState.hero.y}, analyze time = ${System.currentTimeMillis - startTime}")
    (bestTarget.x, bestTarget.y)
  }

  def danger(state: State): Boolean = {
    val stepsInReserve = state.humans.map { h =>
      (state.hero.dist(h), state.zombies.map(z => (z.dist(h), z)).sortBy(_._1).head, h)
    }.map {
      case (dist, (zDist, zombie), human) =>
        (dist.toDouble / HERO_SPEED).ceil.toInt - ((zDist.toDouble - ZOMBIE_SPEED) / ZOMBIE_SPEED).ceil.toInt
    }
    Console.err.println(s"stepsInReserve = ${stepsInReserve}")
    !stepsInReserve.exists(_ < 0)
  }

  def saveNearestStrategy(state: State): (Int, Int) = {
    val survived = state.humans.map { h =>
      (state.hero.distSquare(h), state.zombies.map(z => (z.distSquare(h), z)).sortBy(_._1).head, h)
    }.filter {
      case (distSqr, (zDistSqr, zombie), human) =>
        (distSqr.toDouble / HERO_SPEED_SQR).ceil <= (zDistSqr.toDouble / ZOMBIE_SPEED_SQR).ceil
    }

    val target =
      if (survived.nonEmpty) survived.sortBy(_._1).head._3
      else state.humans.sortBy(_.distSquare(state.hero)).head

    (target.x, target.y)
  }
}

case class Hero(x: Int, y: Int) extends Creature {
  val typeId = 1
  val id = 0
  val isZombie = false
  val isHuman = true
  val isHero = true

  @inline
  def move(target: Creature): Hero = move(target.x, target.y)

  def move(targetX: Int, targetY: Int): Hero = {
    val (x, y) = nextCoord(targetX, targetY, HERO_SPEED)
    this.copy(x = x, y = y)
  }

  @inline
  def canAttack(z: Zombie) = this.distSquare(z) <= HERO_ATTACK_DIST_SQR
}
case class Human(id: Int, x: Int, y: Int) extends Creature {
  val typeId = 2
  val isZombie = false
  val isHuman = true
  val isHero = false
}
case class Zombie(id: Int, x: Int, y: Int/*, nextX: Int, nextY: Int*/) extends Creature {
  val typeId = 3
  val isZombie = true
  val isHuman = false
  val isHero = false

  @inline
  def move(target: Creature): Zombie = move(target.x, target.y)

  def move(targetX: Int, targetY: Int): Zombie = {
    val (x, y) = nextCoord(targetX, targetY, ZOMBIE_SPEED)
    this.copy(x = x, y = y)
  }

  @inline
  def canAttack(h: Human) = this.distSquare(h) <= ZOMBIE_ATTACK_DIST_SQR
}

trait Creature {
  val id: Int
  val x: Int
  val y: Int

  val typeId: Int
  val isZombie: Boolean
  val isHuman: Boolean
  val isHero: Boolean

  @inline
  def distSquare(c: Creature) = {
    val dx = x - c.x
    val dy = y - c.y
    dx * dx + dy * dy
  }

  @inline
  def dist(c: Creature) = Math.sqrt(distSquare(c))

  @inline
  def nextCoord(targetX: Int, targetY: Int, maxSpeed: Int): (Int, Int) = {
    val dx = targetX - x
    val dy = targetY - y
    val dSquare = dx * dx + dy * dy
    if (dSquare <= maxSpeed * maxSpeed) {
      (x + dx, y + dy)
    } else {
      val ratio = maxSpeed / Math.sqrt(dSquare)
      ((x + dx * ratio).toInt, (y + dy * ratio).toInt)
    }
  }

  override def hashCode(): Int = x << 2 + y << 16 + typeId

  override def equals(obj: scala.Any): Boolean =
    if (obj == null) false
    else this.hashCode() == obj.hashCode()
}

object Creature {
  def selectNearest[T <: Creature, B <: Creature](center: B, creatures: List[T], max: Int): List[T] =
    if (creatures.isEmpty || creatures.size <= max)
      creatures
    else
      creatures.map{c => (center.distSquare(c), c)}.sortBy(_._1).take(max).map(_._2)
}