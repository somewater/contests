require 'set'

class Array
  def sum
    self.inject(&:+)
  end
end

class TrueClass
  def to_i
    1
  end
end

class FalseClass
  def to_i
    0
  end
end


code = STDIN.readline.strip
code_chars = code.each_char.to_a.to_set
num = STDIN.readline.strip.to_i
num.times do
  input = STDIN.readline.strip
  include_chars = 0
  pos_chars = \
    input.each_char.zip(code.each_char).map do |user, orig|
      if user == orig
        1
      else
        include_chars += 1 if code_chars.include?(user)
        0
      end
    end.sum

  STDOUT.puts "#{pos_chars.to_s} #{include_chars.to_s}"
end