require 'set'

class Array
  def sum
    self.inject(&:+)
  end
end

class TrueClass
  def to_i
    1
  end
end

class FalseClass
  def to_i
    0
  end
end

# a - 00
# b - 01
# c - 10
# d - 11

def check_a(r)
  last = nil
  res = 0
  r.each do |b|
    res += 1 if last == 0 && b == 0
    last = b
  end
  res
end

def check_b(r)
  last = nil
  res = 0
  r.each do |b|
    res += 1 if last == 0 && b == 1
    last = b
  end
  res
end

def check_c(r)
  last = nil
  res = 0
  r.each do |b|
    res += 1 if last == 1 && b == 0
    last = b
  end
  res
end

def check_d(r)
  last = nil
  res = 0
  r.each do |b|
    res += 1 if last == 1 && b == 1
    last = b
  end
  res
end

def add_a(r)
  r = r.dup
  if r.empty? || r.last != 0
    r << 0 # c
    r << 0
  else
    r << 0
  end
  r
end

def add_b(r)
  r = r.dup
  if r.empty? || r.last != 0
    r << 0 # c
    r << 1
  else
    r << 1
  end
  r
end

def add_c(r)
  r = r.dup
  if r.empty? || r.last != 1
    r << 1 # b
    r << 0
  else
    r << 0
  end
  r
end

def add_d(r)
  r = r.dup
  if r.empty? || r.last != 1
    r << 1 # b
    r << 1
  else
    r << 1
  end
  r
end

def next_step(r, a, b, c, d, ca ,cb, cc, cd, cache)
  key = [ca, cb, cc, cd].join(',')
  return cache[key] if cache.include?(key)
  return r if ca == a && cb == b && cc == c && cd == d

  plus_c = 0
  plus_b = 0

  if !r.empty? && r.last != 0 && (ca < a || cb < b)
    # need add C
    if cc == c
      cache[key] = nil
      return nil
    else
      plus_c = 1
    end
  elsif !r.empty? && r.last != 1 && (cc < c || cd < d)
    # need add B
    if cb == b
      cache[key] = nil
      return nil
    else
      plus_b = 1
    end
  end

  if ca < a
    r0 = next_step(add_a(r), a, b, c, d, ca + 1, cb, cc + plus_c, cd, cache)
    if r0
      cache[key] = r0
      return r0
    end
  end

  if cb < b
    r0 = next_step(add_b(r), a, b, c, d, ca, cb + 1, cc + plus_c, cd, cache)
    if r0
      cache[key] = r0
      return r0
    end
  end

  if cc < c
    r0 = next_step(add_c(r), a, b, c, d, ca, cb + plus_b, cc + 1, cd, cache)
    if r0
      cache[key] = r0
      return r0
    end
  end

  if cd < d
    r0 = next_step(add_d(r), a, b, c, d, ca, cb + plus_b, cc, cd + 1, cache)
    if r0
      cache[key] = r0
      return r0
    end
  end

  cache[key] = nil
  return nil
end

num = STDIN.readline.strip.to_i
num.times do
  input = STDIN.readline.strip
  a, b, c, d = input.split(' ').map(&:to_i)
  r = []

  resp = next_step(r, a, b, c, d, 0, 0, 0, 0, {})
  if resp
    STDOUT.puts resp.join('')
  else
    STDOUT.puts 'impossible'
  end
end